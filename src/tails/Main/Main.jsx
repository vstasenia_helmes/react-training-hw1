import React, { Component } from 'react';
import EmployeeContainer from "../../employee/EmployeeContainer/EmployeeContainer";
import NewEmployee from "../../employee/NewEmployee/NewEmployee";
// import './Main.css';

export default class Main extends Component {

    constructor(props) {
        super(props);

        this.state = {
            employees: [{
                id: 100,
                name: 'Ivan',
                email: 'ivan@email.tt',
                deleted: false
            }, {
                id: 200,
                name: 'Sam',
                email: 'sam@email.tt',
                deleted: true
            }],
            lastId: 1000
        };

        this.employeesHandler.add = this.employeesHandler.add.bind(this);
        this.employeesHandler.onDelete = this.employeesHandler.onDelete.bind(this);
        this.employeesHandler.isEmployeeExist = this.employeesHandler.isEmployeeExist.bind(this);
        this.employeesHandler.isEmailExist = this.employeesHandler.isEmailExist.bind(this);
    }

    employeesHandler = {
        add(srcName, srcEmail) {
            let name = srcName.trim();
            let email = srcEmail.trim();
            if (!name) {
                this.props.messagesHandler.show(['Name can not be empty']);
            } else if (!email) {
                this.props.messagesHandler.show(['Email can not be empty']);
            } else if (this.employeesHandler.isEmailExist(email)) {
                this.props.messagesHandler.show(['User with such email exists already']);
            } else {
                this.setState(prev => {
                    let newId = prev.lastId + 1;
                    let newEmployees = [...prev.employees];
                    newEmployees.push({
                        id: newId,
                        name: name,
                        email: email,
                        deleted: false
                    });
                    return {
                        employees: newEmployees,
                        lastId: newId
                    }
                })
            }
        },

        onDelete(id) {
            const newEmployees = this.state.employees.filter(e => e.id !== id);
            this.setState({
                employees: newEmployees
            });
        },

        isEmployeeExist(employee) {
            return this.emloyeesHandler.isEmailExist(employee.email);
        },

        isEmailExist(email) {
            let existedEmployee = this.state.employees.find(e => e.email === email);
            return existedEmployee !== undefined;
        }
    };

    addMessage = () => {
        this.props.messagesHandler.add('new message at ' + new Date());
    };

    render() {
        return <div>
            {/*<button onClick={this.props.messagesHandler.clear}>
                clear
            </button>
            <button onClick={this.addMessage}>
                add
            </button>*/}
            <NewEmployee employeesHandler={this.employeesHandler}/>
            <EmployeeContainer employees={this.state.employees} employeesHandler={this.employeesHandler} />
        </div>;
    }

}