import React, {Component} from 'react';

export default class MessagePanel extends Component {

    render() {
        const messages = this.props.messages.map((e, i) =>
            <span key={i << 10 + e.length} className="mdl-chip mdl-chip--contact">
                <span className="mdl-chip__contact mdl-color--red mdl-color-text--white">E</span>
                <span className="mdl-chip__text">{e}</span>
            </span>
        );
        const isEmpty = messages === null || messages.length === 0;

        return isEmpty ? null : (
            <div>
                {messages}
            </div>
        );
    }

}