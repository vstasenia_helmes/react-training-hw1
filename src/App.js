import React, {Component} from 'react';
import './App.css';
import Header from './tails/Header/Header';
import Main from './tails/Main/Main';
import Footer from './tails/Footer/Footer';
import Menu from "./tails/Menu/Menu";
import MessagePanel from "./common/MessagePanel/MessagePanel";

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: []
        };

        this.messagesHandler.clear = this.messagesHandler.clear.bind(this);
        this.messagesHandler.add = this.messagesHandler.add.bind(this);
        this.messagesHandler.show = this.messagesHandler.show.bind(this);
    }

    messagesHandler = {
        clear() {
            this.setState({
                messages: []
            })
        },

        add(msg) {
            this.setState(prev => {
                let newMessages = [...prev.messages];
                newMessages.push(msg);
                return {
                    messages: newMessages
                };
            })
        },

        show(msgs) {
            this.setState({
                messages: msgs
            })
        }
    };

    render() {
        return (
            <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
                <Header />
                <Menu />
                <main className="mdl-layout__content">
                    <div className="page-content">
                        <div className='mdl-grid'>
                            <div className='mdl-cell mdl-cell--12-col mdl-cell--middle'>
                                <MessagePanel messages={this.state.messages} />
                            </div>
                            <div className='mdl-cell mdl-cell--12-col mdl-cell--middle'>
                                <Main messagesHandler={this.messagesHandler} />
                            </div>
                        </div>
                    </div>
                </main>
                {/*<Footer/>*/}
            </div>
        );
    }
}

export default App;
