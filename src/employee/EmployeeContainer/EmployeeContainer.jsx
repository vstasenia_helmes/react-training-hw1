import React, { Component } from 'react';
import Employee from "../Employee/Employee";
import './EmployeeContainer.css';

export default class EmployeeContainer extends Component {

    render() {
        let employees = this.props.employees.map(e => e.deleted ? null :
            <Employee key={e.id} employee={e} employeesHandler={this.props.employeesHandler} />
        );

        return <table className='employee-table mdl-data-table mdl-js-data-table mdl-shadow--2dp'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th className='mdl-data-table__cell--non-numeric'>Name</th>
                    <th className='mdl-data-table__cell--non-numeric'>Email</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {employees}
                {this.children}
            </tbody>
        </table>
    }

}