import React, { Component } from 'react';
import './Footer.css';

export default class Footer extends Component {

    render() {
        return <footer className="mdl-mega-footer">
            <div className="mdl-mega-footer__bottom-section">
                <div className="mdl-logo">Title</div>
                <ul className="mdl-mega-footer__link-list">
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Privacy & Terms</a></li>
                </ul>
            </div>
        </footer>;
    }

}