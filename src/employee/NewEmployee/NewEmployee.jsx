import React, { Component } from 'react';
// import './NewEmployee.css';

export default class NewEmployee extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: ''
        };

        this.addEmployee = this.addEmployee.bind(this);
        this.onFieldChange = this.onFieldChange.bind(this);
    }

    addEmployee() {
        this.props.employeesHandler.add(this.state.name, this.state.email);
    };

    onFieldChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    render() {
        return <div>
            <div className="mdl-textfield mdl-js-textfield">
                <input type='text' className="mdl-textfield__input"
                       name='name' value={this.state.name}
                       onChange={this.onFieldChange} />
                <label className="mdl-textfield__label" htmlFor="sample1">Name</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield">
                <input type='text' className="mdl-textfield__input"
                       name='email' value={this.state.email}
                       onChange={this.onFieldChange} />
                <label className="mdl-textfield__label" htmlFor="sample1">Email</label>
            </div>
            <button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                    onClick={this.addEmployee}>
                add
            </button>
        </div>
    }

}