import React, { Component } from 'react';
// import './Employee.css';

export default class Employee extends Component {

    render() {
        const e = this.props.employee;
        return <tr>
            <td>
                {e.id}
            </td>
            <td className='mdl-data-table__cell--non-numeric'>
                {e.name}
            </td>
            <td className='mdl-data-table__cell--non-numeric'>
                {e.email}
            </td>
            <td>
                <button className="mdl-button mdl-js-button mdl-button--icon" onClick={event => this.props.employeesHandler.onDelete(e.id)}>
                    <i className="material-icons">delete</i>
                </button>
            </td>
        </tr>
    }

}